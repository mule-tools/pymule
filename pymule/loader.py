import re
from .vegas import importvegas, getplots
from .errortools import *
import os
import inspect
import tarfile
import io
import shutil
import hashlib

loadargs = {}


def hash_file(name):
    """
    hashes a files using SHA1

    :param name:
        file path

    :return:
        hex-digested SHA1 hash of the file
    """
    m = hashlib.sha1()
    with open(name, 'rb') as fp:
        while True:
            data = fp.read(65536)
            if not data:
                break
            m.update(data)
        sha1hash = m.hexdigest()
    return sha1hash


def commit_cache(cachefolder, full_name, fp):
    """
    writes to cache if cachefolder exists

    :param cachefolder:
        path to cache folder

    :param full_name:
        name of file in cache folder

    :param fp:
        file pointer to read from
    """
    if os.path.isdir(cachefolder):
        with open(full_name, 'wb') as cfp:
            shutil.copyfileobj(fp, cfp)
        fp.seek(0)


def importreg(r, folder='.', filenames=None,
              cachefolder="",
              merge={}, types=[int,float,float],
              sanitycheck=lambda x: True):
    """
    imports all vegas files matching a regular expression

    :param r:
        str;
        regular expression to match in file names

    :param folder:
        str, optional;
        file name, optional;
        folder or tarball to search for vegas files

    :param filenames:
        list, optional;
        list of files to loads, defaults to all files in **folder** (recurisvely if tar ball)

    :param cachefolder:
        folder name, optional;
        if existing folder, use as cache for compressed tarballs

    :param merge:
        dict, optional:
        a dict of histograms ``{'name': n}`` to merge ``n`` bins in the histogram ``name``.
        defaults to no merging

    :param types:
        list of callables, optional;
        functions that convert the groups matched by ``r`` into python objects.
        Common examples would be ``int`` or ``float``.
        Default: ``[int,float, float]`` as per McMule filename convention

    :param sanitycheck:
        callable, optional;
        a function that, given a vegas dict, whether to include the file in the output (return ``True``) or to skip (return ``False``).

    :return:
        a dictionary of merged vegas datasets, keyed by the groups defined in the regular expression ``r`` as parsed by ``types``
    """

    tar = None
    if not os.path.isdir(folder):
        full_name = os.path.join(cachefolder, hash_file(folder))
        if os.path.isdir(cachefolder) and os.path.isfile(full_name):
            tar = tarfile.open(full_name)
        elif os.path.splitext(folder)[1] == '.bz2':
            import bz2
            with bz2.BZ2File(folder, 'r') as b:
                fp = io.BytesIO(b.read())
            commit_cache(cachefolder, full_name, fp)
            tar = tarfile.open(fileobj=fp)

        elif os.path.splitext(folder)[1] == '.gz':
            import gzip
            with gzip.open(folder, 'r') as b:
                fp = io.BytesIO(b.read())
            commit_cache(cachefolder, full_name, fp)
            tar = tarfile.open(fileobj=fp)
        else:
            tar = tarfile.open(folder)

    if filenames is None:
        if tar:
            filenames = [
                i.name
                for i in tar if 'vegas' in i.name
            ]
        else:
            filenames = os.listdir(folder)

    dic = {}
    for filename in filenames:
        m = re.match(r, filename)
        if m:
            if tar:
                v = importvegas(fp=tar.extractfile(filename))
            else:
                v = importvegas(os.path.join(folder, filename))

            if not sanitycheck(v):
                continue

            for p, n in merge.items():
                if n > 0:
                    v[p] = mergebins(v[p], n)

            if len(types) == len(m.groups()):
                k = tuple(f(v) for v,f in zip(m.groups(), types))
            else:
                k = m.groups()
            dic[k] = v

    return dic


def pattern(piece='.*', flavour='.*', obs='', folderp='.*'):
    """
    constructs a regular expression to be used in :func:`~pymule.importreg` matching the usual McMule file name convention

    :param piece:
        str, optional;
        the ``which_piece`` to load, defaults to everything

    :param flavour:
        str, optional;
        the ``flavour`` to load, defaults to everything

    :param obs:
        str, optional;
        the observable to load (the bit after the ``O``), defaults to everything

    :param folderp:
        str, optional;
        a regular expression to match directory structures of a tar file, defaults to everything

    :return:
        a regular expression to be used in :func:`~pymule.importreg`
    """
    if len(obs) > 0:
        return (
            folderp + piece + '_' + flavour + r'_S(\d+)X([\d\.]+)D([\d\.]+).*O'
            + obs + '.vegas'
        )
    else:
        return (
            folderp + piece + '_' + flavour + r'_S(\d+)X([\d\.]+)D([\d\.]+).*'
        )


def mergeset(s, binwisechi=False):
    """
    statistically merges a set of runs, combining cross sections, histograms, and run-time information

    :param s:
        a list of vegas datasets

    :param binwisechi:
        bool, optional;
        whether to include the bin-wise :math:`\chi^2` in the result.

    :raises:
        KeyError 'time': if merge is unsucessfull because no data is found

    :todo: make error handling more useful

    :return:
        a merged vegas dataset.
        The runtime is the sum of individual times.
        The :math:`\chi^2` is a list of

         * the :math:`\chi^2` of the cross section combination
         * a list of the individual :math:`\chi^2`
    """
    if len(s) == 0:
        return {}
    if len(s) == 1:
        return s[0]

    c, v = mergenumbers([i['value'] for i in s],True)
    dic = {
        'time': sum([i['time'] for i in s]),
        'value': v,
        'chi2a': [c, [i['chi2a'] for i in s]]
    }

    for plot in getplots(s):
        if binwisechi:
            p, chi = mergeplots([i[plot] for i in s], returnchi=True)
            if p.shape == (0,):
                continue
            dic[plot] = p
            dic[plot + '__chisq'] = np.column_stack((
                chi,np.zeros(chi.shape[0])
            ))
        else:
            p = mergeplots([i[plot] for i in s])
            if p.shape == (0,):
                continue
            dic[plot] = p

    return dic


def mergeseeds(s, key=lambda x: (x[1], x[2])):
    r"""
    statistically merges the different random seeds of a number of runs, combining cross sections, histograms, and run-time information.

    :param s:
        a list of vegas datasets

    :param key:
        callable;
        function to define the keys of the resulting dictionary.
        Usually, this refers to the FKS parameters.
        In the default notation this is ``lambda x: (x[1], x[2])`` referring to :math:`\xi_c` and :math:`\delta`, resp.

    :raises:
        KeyError 'time': if merge is unsucessfull because no data is found

    :todo: make error handling more useful

    :return:
        a merged vegas dataset.
        The runtime is the sum of individual times.
        The :math:`\chi^2` is a list of

         * the :math:`\chi^2` of the cross section combination
         * a list of the individual :math:`\chi^2`
    """
    groups = set(map(key, s.keys()))

    dic = {}
    for group in groups:
        ss = [
            s[k]
            for k in s.keys()
            if key(k) == group
        ]
        dic[group] = mergeset(ss)

    return dic


def addsets(s):
    """
    adds a list of vegas datasets

    :param s:
        a list of vegas datasets dictionaries with the keys
          * time
          * value
          * chi2a
          * all histograms as specified by their ``name(..)`` in user.f95

    :return:
        the resulting sum. The resulting :math:`\chi^2` is a list of constituent :math:`\chi^2`.
    """
    if len(s) == 0:
        return {}

    dic = {
        'time': sum([i['time'] for i in s]),
        'value': plusnumbers([i['value'] for i in s]),
        'chi2a': [i['chi2a'] for i in s]
    }

    for plot in getplots(s):
        dic[plot] = combineNplots(addplots, [i[plot] for i in s])

    return dic


def scaleset(s, v):
    """
    rescales a vegas dataset

    :param s:
        a vegas datasets dictionaries with the keys
          * time
          * value
          * chi2a
          * all histograms as specified by their ``name(..)`` in user.f95

    :param v:
        the value to rescale the y values.

    :return:
        the rescaled dataset

    .. note::
        This naturally changes the cross section
    """
    dic = {
        'time': s['time'],
        'chi2a': s['chi2a'],
        'value': v * s['value']
    }
    for plot in getplots(s):
        dic[plot] = scaleplot(s[plot], 1., v)
    return dic


def scalesets(s, v):
    """
    rescales a list of vegas datasets

    :param s:
        a list of vegas datasets dictionaries with the keys
          * time
          * value
          * chi2a
          * all histograms as specified by their ``name(..)`` in user.f95

    :param v:
        the value to rescale the y values.

    :return:
        all rescaled datasets

    .. note::
        This naturally changes the cross section
    """

    return {
        i: scaleset(s[i], v)
        for i in s.keys()
    }


def multiintersect(lists):
    """
    finds elements that are common to all lists.
    This is used to find a list of FKS parameters of a given run.

    :param list:
        list of lists :math:`l_1`, :math:`l_2`, ..., :math:`l_n`

    :return:
        the list :math:`l_1 \cap l_2 \cap \cdots \cap l_n`
    """
    if len(lists) == 0:
        return []
    inter = set(lists[0])
    for i in lists[1:]:
        inter = inter.intersection(set(i))
    return list(inter)


def mergefks(*sets, **kwargs):
    r"""
    performs the FKS merge

    :param sets:
        random-seed-merged results (usually from :func:`~pymule.sigma`)

    :param binwisechi:
        bool, optional, default False;
        if set to True, also return extra distributions containing the :math:`\chi^2` of the bin-wise FKS merge.
        This cannot be used together with ``anyxi`` and the result should *not* be passed to ``scaleset`` for obvious reasons.


    :return:
        the FKS-merged final set containing cross sections, distributions, and run-time information.
        The chi2a return is a list of the following

         * the :math:`\chi^2` of the FKS merge
         * a list of :math:`\chi^2` from previous operations, such as random seed merging or the integration.

    .. note::
        Optional argument ``anyxi`` (or anything starting with ``anyxi``):
        Sometimes it is necessary to merge :math:`\xi_c`-dependent runs (such as a counter term) and :math:`\xi_c`-independent runs (such as the one-loop term).
        Do not use this together with **binwisechi**

    :Example:
        Load the LO results for the muon decay using :func:`~pymule.sigma`

        >>> mergefks(sigma("m2enn0"))

        Load the NLO results

        >>> mergefks(sigma("m2ennV"), sigma("m2ennR"))

        Load the NNLO results where ``m2ennNF`` does not depend on :math:`\xi_c`

        >>> mergefks(sigma("m2ennFF"), sigma("m2ennRF"), sigma("m2ennRR"), anyxi=sigma("m2ennNF"))
    """
    paras = multiintersect([i.keys() for i in sets])

    anyxi = []
    for i in kwargs.keys():
        if 'anyxi' not in i:
            continue
        if len(kwargs[i].keys()) > 1:
            raise KeyError('%s has multiple xis! Only one is allowed' % i)
        anyxi.append(list(kwargs[i].values())[0])

    ans = []
    for para in paras:
        ans.append(addsets([i[para] for i in sets]))

    if 'binwisechi' in kwargs and kwargs['binwisechi']:
        if len(anyxi) > 0:
            raise KeyError(
                "anyxi cannot be used together with binwisechi=True"
            )
        return mergeset(ans, binwisechi=True)

    return addsets([mergeset(ans)] + anyxi)


def callsanitised(func, **kwargs):
    """
    calls a function with arguments from ``kwargs`` and those specified in ``loadargs``

    :param func:
        callable;
        function to call

    :param **kwargs:
        arguments overriding ``loadargs``

    Arguments that don't match **func** are discarded.
    """
    global loadargs
    legal = inspect.getfullargspec(func).args
    args = {}
    args.update(loadargs)
    args.update(kwargs)
    args = {
        i: args[i]
        for i in args.keys()
        if i in legal
    }
    return func(**args)


def setup(**kwargs):
    """
    sets the default arguemnts for :func:`~pymule.sigma`.

    :param folder:
        str, optional;
        file name, optional;
        folder or tarball to search for vegas files
        Initialised to current directory (``.``).

    :param flavour:
        str, optional;
        the ``flavour`` to load, defaults to everything
        Initialised to everything, i.e. ``.*``.

    :param obs:
        str, optional;
        the observable to load (the bit after the ``O``), defaults to everything
        Initialised to everything, i.e. ``''``.

    :param folderp:
        str, optional;
        a regular expression to match directory structures of a tar file, defaults to everything
        Initialised to everything, i.e. ``.*``.

    :param filenames:
        list, optional;
        list of files to loads, defaults to all files in **folder** (recurisvely if tar ball)
        Initialised to ``None``, meaning everything.

    :param merge:
        dict, optional:
        a dict of histograms ``{'name': n}`` to merge ``n`` bins in the histogram ``name``.
        Initialised to to no merging, i.e. ``{}``

    :param types:
        list of callables, optional;
        functions that convert the groups matched by ``r`` into python objects.
        Common examples would be ``int`` or ``float``.
        Initialised to ``[int,float, float]`` as per McMule filename convention.

    :param sanitycheck:
        callable, optional;
        a function that, given a vegas dict, whether to include the file in the output (return ``True``) or to skip (return ``False``).
        Initialised to ``lambda x : True``, i.e. include everything.

    :param cache:
        folder name, optional;
        if existing folder, use as cache for compressed tarballs

    :Example:
        Setup some folders, ensure that ``/tmp/mcmule`` exists

        >>> setup(folder="path/to/data.tar.bz2", cachefolder="/tmp/mcmule")

    :Example:
        Restrict observable

        >>> setup(obs="3")

    :Example:
        Drop runs with a :math:`\chi^2 > 10`

        >>> setup(sanitycheck=lambda x : x['chi2a'] < 10)

    """
    global loadargs
    loadargs.update(kwargs)


def sigma(piece, **kwargs):
    """
    loads a ``which_piece`` and statistically combines the random seed.

    :parm piece:
        str;
        ``which_piece`` to load

    :param folder:
        str, optional;
        file name, optional;
        folder or tarball to search for vegas files
        Initialised to current directory (``.``).

    :param flavour:
        str, optional;
        the ``flavour`` to load, defaults to everything
        Initialised to everything, i.e. ``.*``.

    :param obs:
        str, optional;
        the observable to load (the bit after the ``O``), defaults to everything
        Initialised to everything, i.e. ``''``.

    :param folderp:
        str, optional;
        a regular expression to match directory structures of a tar file, defaults to everything
        Initialised to everything, i.e. ``.*``.

    :param filenames:
        list, optional;
        list of files to loads, defaults to all files in **folder** (recurisvely if tar ball)
        Initialised to ``None``, meaning everything.

    :param merge:
        dict, optional:
        a dict of histograms ``{'name': n}`` to merge ``n`` bins in the histogram ``name``.
        Initialised to to no merging, i.e. ``{}``

    :param types:
        list of callables, optional;
        functions that convert the groups matched by ``r`` into python objects.
        Common examples would be ``int`` or ``float``.
        Initialised to ``[int,float, float]`` as per McMule filename convention.

    :param sanitycheck:
        callable, optional;
        a function that, given a vegas dict, whether to include the file in the output (return ``True``) or to skip (return ``False``).
        Initialised to ``lambda x : True``, i.e. include everything.

    :param cache:
        folder name, optional;
        if existing folder, use as cache for compressed tarballs

    :return:
        a dict with the tuples of FKS parameters as keys and vegas datasets as values.

    .. note::
        Use :func:`~pymule.setup` to set the defaults.
        Arguments provided here override the defaults

    :Example:
        Load the leading order muon decay

        >>> sigma("m2enn0")

        Load only observable ``O3``

        >>> sigma("m2enn0", obs="3")
    """
    global loadargs
    pat = callsanitised(pattern, piece=piece, **kwargs)
    s = callsanitised(importreg, r=pat, **kwargs)
    return callsanitised(mergeseeds, s=s, **kwargs)
