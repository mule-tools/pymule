import numpy as np


def chisq(values):
    r"""
    calculates the :math:`\chi^2/\textrm{d.o.f.}` of numbers

    :param value:
        Nx2 numpy matrix or list of lists;
        the values as ``[[y1, dy1], [y2, dy2], ...]``

    :return:
        float;
        the :math:`\chi^2/\textrm{d.o.f.} = \frac{1}{n} \sum_{n=1}^n(\frac{y_i-\bar y}{\delta y_i})^2` with the average value :math:`\bar y`

    :Example:
        a good example

        >>> chisq([[20.0, 0.8],
        ...        [21.6, 0.9],
        ...        [18.7, 1.2]])
        1.3348808062205872

        and a bad example

        >>> chisq([[16.2, 0.8],
        ...        [22.9, 0.9],
        ...        [8.81, 1.2]])
        30.173852184366673
    """
    if type(values) == list:
        values = np.array(values)
    weight = sum(1 / values[:,1]**2)
    value = sum(values[:,0] / values[:,1]**2 / weight)
    chi = 1./len(values) * sum(
        ((values[:,0]-value) / values[:,1])**2
    )
    return chi


def mergenumbers(values, quiet=False):
    """
    statistically combines values with uncertainties

    :param values:
        Nx2 numpy matrix or list of lists;
        the values as ``[[y1, dy1], [y2, dy2], ...]``

    :param quiet:
        bool, optional;
        whether to print or return the :math:`\chi^2` for the combination

    :return:
        either answer as numpy array ``[y, dy]`` or tuple of :math:`chi^2` and answer

    :Example:
        If ``quiet`` is not specified this will print the :math:`chi^2`

        >>> mergenumbers([[20.0, 0.8],
        ...               [21.6, 0.9],
        ...               [18.7, 1.2]])
        1.3348808062205872
        array([20.30718232,  0.53517179])

        Otherwise, it will return it

        >>> mergenumbers([[20.0, 0.8],
        ...               [21.6, 0.9],
        ...               [18.7, 1.2]], quiet=True)
        (1.3348808062205872, array([20.30718232,  0.53517179]))
    """
    if type(values) == list:
        values = np.array(values)

    weight = sum(1 / values[:,1]**2)
    value = sum(values[:,0] / values[:,1]**2 / weight)
    chi = 1./len(values) * sum(
        ((values[:,0]-value) / values[:,1])**2
    )
    ans = np.array([value, np.sqrt(1/weight)])

    if not quiet:
        print(chi)
        return ans
    else:
        return chi, ans


def plusnumbers(*args):
    r"""
    adds numbers and errors

    :param yi:
        list of floats;
        a number with error ``[yi, dyi]``

    :return:
        the result of the addition ``[y, dy]``

    :Example:
        Adding :math:`(10\pm1)+(20\pm0.5)+(-5\pm2)`

        >>> plusnumbers([10, 1], [20, 0.5], [-5, 2])
        array([25.        ,  2.29128785])
    """
    if len(args) == 1:
        lst = np.array(args[0])
    else:
        lst = np.array(args)
    return np.array([
        sum(lst[:,0]),
        np.sqrt(sum(lst[:,1]**2))
    ])


def dividenumbers(a, b):
    r"""
    divides numbers

    :param a:
        list of floats;
        the numerator with error ``[a, da]``

    :param b:
        list of floats;
        the denominator with error ``[b, db]``

    :return:
        the result of the division a/b ``[y, dy]``

    :Example:
        Divide :math:`(2.3\pm0.1) / (45\pm0.01)`

        >>> dividenumbers([2.3, 0.1], [45., 0.01])
        array([0.05111111, 0.00222225])
    """
    return np.array([
        a[0]/b[0],
        np.sqrt((b[1]**2*a[0]**2)/b[0]**4 + a[1]**2/b[0]**2)
    ])


def timesnumbers(a, b):
    r"""
    multiplies numbers

    :param a:
        list of floats;
        the first factor with error ``[a, da]``

    :param b:
        list of floats;
        the second factor with error ``[b, db]``

    :return:
        the result of the multiplication a*b ``[y, dy]``

    :Example:
        Divide :math:`(0.5\pm0.02) * (45\pm0.01)`

        >>> timesnumbers([0.5,0.02], [45, 0.1])
        array([22.5       ,  0.90138782])
    """
    return np.array([
        a[0]*b[0],
        np.sqrt(b[0]**2*a[1]**2 + a[0]**2*b[1]**2)
    ])


def integratehistogram(hist):
    """
    integrates a histogram

    :param hist:
        Nx3 numpy matrix;
        the histogram to integrate :math:`d\sigma/dx` as ``np.array([[x1, y1, e1], [x2, y2, e2], ...])``

    :return:
        float;
        the integrated histogram :math:`\int d\sigma/dx dx` without error estimate

    :Example:
        Integrate a histogram

        >>> hist
        array([[          -inf, 0.00000000e+00, 0.00000000e+00],
               [5.00000000e-02, 4.77330751e+01, 2.26798977e-01],
               [1.50000000e-01, 7.40641192e+01, 2.36498021e-01],
               ...,
               [8.85000000e+00, 1.67513948e+00, 1.16218116e-01],
               [8.95000000e+00, 0.00000000e+00, 0.00000000e+00],
               [           inf, 0.00000000e+00, 0.00000000e+00]])
        >>> integratehistogram(hist)
        4188.519369660588
    """
    ans = 0
    if hist[0,0] == -np.inf:
        ans += hist[0,1]
        hist = hist[1:]
    if hist[-1,0] == +np.inf:
        ans += hist[-1,1]
        hist = hist[:-1]

    # The histogram might have multiple bin sizes..
    ind = list(np.where(~np.isclose(np.diff(hist[:,0],2),0))[0] + 2)
    if len(ind) == 0:
        # all bins are the same size, continue
        ans += np.mean(np.diff(hist[:,0])) * sum(hist[:,1])
    else:
        ind = [0] + ind[::2] + [len(hist)]
        for i,j in zip(ind, ind[1:]):
            d = np.diff(hist[i:j, 0])
            if not np.all(np.isclose(d[0], d)):
                raise ValueError(
                    "Even after splitting, not all d are identical"
                )

            ans += d[0] * sum(hist[i:j,1])
    return ans


def mergeplots(ps, returnchi=False):
    """
    statistically combines a list of plots

    :param ps:
        list of Nx3 numpy matrices;
        the plots to combine as ``[np.array([[x1, y1, e1], [x2, y2, e2], ...]), ...]``

    :param returnchi:
        bool, optional;
        if ``True`` returns two plots, the requested combination and the bin-wise :math:`\chi^2`

    :return:
        a Nx3 numpy matrix if ``returnchi=False``

    :Example:
        Load a number of ``vegas`` files and merge them

        >>> data = [
        ...     importvegas(i)['thetae']
        ...     for i in glob.glob('out/em2em0*')
        ... ]
        >>> mergeplots(data)
    """
    if type(ps) == list:
        ps = np.array(ps)

    if len(ps.shape) != 3:
        return np.array([])
    with np.errstate(divide='ignore', invalid='ignore'):
        w = np.sum(1/ps[:,:,2]**2,0)
        out = np.zeros(ps[0].shape)

        out[:,1] = np.sum(ps[:,:,1]/ps[:,:,2]**2 / w, 0)
        out[:,2] = np.sqrt(1/w)

        # if one bin is empty, the zero causes trouble
        out[~np.isfinite(out)] = 0
        out[:,0] = ps[0,:,0]

        chi = 1./(len(ps)-1)*np.sum((ps[:,:,1]-out[:,1])**2/ps[:,:,2]**2,0)
        chi[~np.isfinite(chi)] = 0
        chi = np.column_stack((out[:,0], chi))

    if returnchi:
        return out, chi
    else:
        return out


def combineplots(a, b, yfunc, efunc, tol=1e-8):
    """
    combines two plots using functions for the value and the error

    :param a:
        Nx3 numpy matrix;
        the first plot

    :param b:
        Nx3 numpy matrix;
        the second plot

    :param yfunc:
        callable;
        a function to calculate the value ``yfunc(a, b)``

    :param efunc:
        callable;
        a function to calculate the error ``efunc(a, da b, db)``

    :param tol:
        float, optional;
        the difference at which values are considered equal

    :return:
        Nx3 numpy matrix;
        the combined plot ``np.array([[x1, yfunc(..), efunc(..)], ..])``

    .. note::
        a and b must share x values, up to the tolerance ``tol``, otherwise values may be dropped

    :Example:
        Add two plots ``A`` and ``B``

        >>> combineplots(A, B,
        ...              lambda a, b: a+b,
        ...              lambda a, da, b, db: sqrt(da**2 + db**2))

        Calculate a K factor

        >>> combineplots(dnlo, lo,
        ...              lambda a, b: 1 + a/b,
        ...              lambda a, da, b, db: np.sqrt(db**2 * a**2 / b**4 + da**2 / b**2)
    """
    la = np.round(a[:,0] / tol)
    lb = np.round(b[:,0] / tol)
    newx = np.intersect1d(la, lb)

    maskA = np.in1d(la, newx)
    maskB = np.in1d(lb, newx)

    with np.errstate(divide='ignore', invalid='ignore'):
        x = a[maskA, 0]
        y = yfunc(a[maskA, 1], b[maskB, 1])
        e = efunc(a[maskA, 1], a[maskA, 2], b[maskB, 1], b[maskB, 2])

        y[~np.isfinite(y)] = 0
        e[~np.isfinite(e)] = 0

        out = np.column_stack((x, y, e))
        # if one bin is empty, the zero causes trouble

    return out


def addplots(a, b, sa=1., sb=1.):
    """
    adds or subtracts two plots

    :param a:
        Nx3 numpy matrix;
        the first plot

    :param b:
        Nx3 numpy matrix;
        the second plot

    :param sa:
        float, optional;
        the coefficient of the first plot

    :param sb:
        float, optional;
        the coefficient of the second plot

    :return: a Nx3 numpy matrix with :math:`s_a\cdot a + s_b\cdot b`

    .. note::
        a and b must share x values, otherwise entries are dropped

    :Example:
        subtract two plots ``a`` and ``b``

        >>> addplots(a, b, sb=-1)

    :Example:
        Given the LO plots ``thetaLO`` and the NLO corrections ``thetadNLO``, we calculate the :math:`K` factor as either

        >>> thetaNLO = addplots(thetaLO, thetadNLO)
    """
    return combineplots(
        a, b,
        lambda y1, y2: sa*y1 + sb*y2,
        lambda y1, e1, y2, e2: np.sqrt((sa*e1)**2 + (sb*e2)**2)
    )


def divideplots(a, b, offset=0.):
    """
    divides two plots

    :param a:
        Nx3 numpy matrix;
        the numerator plot

    :param b:
        Nx3 numpy matrix;
        the denominator plot

    :param offset:
        float, optional;
        shifts the result

    :return: a Nx3 numpy matrix with :math:`a/b + offset`

    .. note::
        a and b must share x values, otherwise entries are dropped

    :Example:
        Given the LO plots ``thetaLO`` and the NLO corrections ``thetadNLO``, we calculate the :math:`K` factor as either

        >>> thetaNLO = addplots(thetaLO, thetadNLO)
        >>> thetaK = divideplots(thetaNLO, thetaLO)
        >>> thetaK = divideplots(thetadNLO, thetaLO, offset=+1.)
    """
    return combineplots(
        a, b,
        lambda y1, y2: offset + y1 / y2,
        lambda y1, e1, y2, e2: np.sqrt(e2**2 * y1**2 / y2**4 + e1**2 / y2**2)
    )


def combineNplots(func, plots):
    """
    combines a list of plots using a function

    :param func:
        callable with two arguments;
        the function to combine the plots

    :param plots:
        list of Nx3 numpy matrices

    :return:
        a Nx3 numpy matrix :math:`f(p_0, f(p_1, f(p_2, \cdots)))`
    """
    accum = plots[0]
    for plot in plots[1:]:
        accum = func(accum, plot)
    return accum


def scaleplot(a, sx, sy=None):
    r"""
    rescales a plot such that the integrated plot remains unchanged, i.e. rescale :math:`x\to x/s` and :math:`y\to y\cdot s`.
    This is useful to, for example, change units.

    :param a:
        Nx3 numpy matrix;
        the plot

    :param sx:
        float;
        the inverse scale factor for the x direction

    :param sy:
        float, optional;
        if present, ``sy`` will be used for the y direction instead of ``sx``

    :return:
        a Nx3 numpy matrix

    :Example:
        rescaling units from rad to mrad

        >>> scaleplot(data, 1e-3)
    """
    if sy is None:
        return np.array((1./sx, sx, sx)) * a
    else:
        return np.array((1./sx, sy, sy)) * a


def mergebins(p, n):
    """
    merges n adjacent bins into one larger bin, reducing the uncertainty.

    :param p:
        Nx3 numpy matrix;
        the plot

    :param n:
        int;
        how many bins to merge

    :return:
        a (N/n)x3 numpy matrix

    .. note::
        This process loses ``len(p)%n`` bins at the end of the histogram

    :Example:
        merge five bins

        >>> len(p)
        200
        >>> len(mergebins(p, 5))
        40

        Bins may be lost

        >>> len(p)
        203
        >>> len(mergebins(p, 5))
        40
    """
    if p[0,0] == -np.inf:
        return np.concatenate([
            [p[0,:]],
            mergebins(p[1:,:], n)
        ])
    if p[-1,0] == +np.inf:
        return np.concatenate([
            mergebins(p[:-1,:], n),
            [p[-1,:]]
        ])

    # Make sure the length fits
    if len(p) % n:
        short = p[:-(len(p) % n)]
    else:
        short = p[:]
    part = np.split(short, len(short)/n)
    return np.array([
        (
            sum(i[:,0])/n,
            sum(i[:,1])/n,
            np.sqrt(sum(i[:,2]**2))/n
        ) for i in part
    ])


def printnumber(x, prec=0):
    """
    returns a string representation of a number with uncertainties to one significant digit

    :param x:
        a list with two floats;
        the number as ``[x, dx]``

    :param prec:
        int, otpional;
        number of extra signficant figures

    :return:
        str;
        the formatted string

    :Example:
        printing :math:`53.2\pm0.1` to one significant figure

        >>> printnumber([53.2, 0.1])
        "53.2(1)"
    """
    if x[1] == 0:
        return x[0]

    digits = int(-np.floor(np.log10(x[1]))) + prec

    fmt = '%.' + str(digits) + 'f(%.' + str(prec) + 'f)'
    return fmt % (
        round(x[0], digits),
        10**digits*round(x[1], digits)
    )
