import argparse
from mesonbuild.scripts.depscan import *
import mesonbuild.mesonmain
import os
import sys


class MyScanner(DependencyScanner):
    def scan(self) -> int:
        for s in self.sources:
            self.scan_file(s)
        with open(self.outfile, 'w', encoding='utf-8') as ofile:
            ofile.write('ninja_dyndep_version = 1\n')
            for src in self.sources:
                objfilename = self.objname_for(src)
                mods_and_submods_needed = []
                module_files_generated = []
                module_files_needed = []
                if src in self.sources_with_exports:
                    module_files_generated.append(self.module_name_for(src))
                if src in self.needs:
                    for modname in self.needs[src]:
                        if modname not in self.provided_by:
                            # Nothing provides this module, we assume that it
                            # comes from a dependency library somewhere and is
                            # already built by the time this compilation starts.
                            pass
                        else:
                            mods_and_submods_needed.append(modname)

                for modname in mods_and_submods_needed:
                    provider_src = self.provided_by[modname]
                    provider_modfile = self.module_name_for(provider_src)
                    # Prune self-dependencies
                    if provider_src != src:
                        module_files_needed.append(provider_modfile)

                quoted_objfilename = ninja_quote(objfilename, True)
                quoted_module_files_generated = [ninja_quote(x, True) for x in module_files_generated]
                quoted_module_files_needed = [ninja_quote(x, True) for x in module_files_needed]
                if quoted_module_files_generated:
                    mod_gen = '| ' + ' '.join(quoted_module_files_generated)
                else:
                    mod_gen = ''
                if quoted_module_files_needed:
                    mod_dep = '| ' + ' '.join(quoted_module_files_needed)
                else:
                    mod_dep = ''
                build_line = 'build {} {}: dyndep {}'.format(quoted_objfilename,
                                                             mod_gen,
                                                             mod_dep)
                ofile.write(build_line + '\n')
                ofile.write('  restat = 1\n')
        return 0


def create_parser(subparsers):
    parser = subparsers.add_parser(
        'meson',
        description='pymule meson wrapper'
    )
    parser.set_defaults(func=main)
    parser.add_argument('--internal')
    parser.add_argument('--capture', action='store_true')
    parser.add_argument('args', nargs=argparse.REMAINDER)


def main(parsed):
    launcher = os.path.realpath(sys.argv[0])
    if not launcher.endswith('-meson'):
        launcher = launcher + '-meson'

    if parsed.internal == 'depscan':
        pickle_file, outfile, jsonfile = parsed.args
        with open(jsonfile, encoding='utf-8') as f:
            sources = json.load(f)
        scanner = MyScanner(pickle_file, outfile, sources)
        scanner.scan()
    elif parsed.internal is None:
        sys.exit(mesonbuild.mesonmain.run(parsed.args, launcher))
    else:
        opts = [
            '--internal', parsed.internal
        ]
        if parsed.capture:
            opts += ['--capture']
        opts += parsed.args
        sys.exit(mesonbuild.mesonmain.run(opts, launcher))


def _main():
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=main)
    parser.add_argument('--internal')
    parser.add_argument('--capture', action='store_true')
    parser.add_argument('args', nargs=argparse.REMAINDER)
    parsed = parser.parse_args()
    main(parsed)
