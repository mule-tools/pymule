import argparse
import pymule.createrun  as createrun
import pymule.manageruns as manageruns
import pymule.switcheroo as switcheroo
import pymule.ffilter    as ffilter
import pymule.imagebuild as imagebuild
import pymule.meson      as meson


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--version', action="store_true")
    subparsers = parser.add_subparsers()

    createrun.create_parser(subparsers)
    manageruns.create_dup_parser(subparsers)
    manageruns.create_rm_parser(subparsers)
    manageruns.create_backup_parser(subparsers)
    switcheroo.create_parser(subparsers)
    ffilter.create_parser(subparsers)
    imagebuild.create_parser(subparsers)
    meson.create_parser(subparsers)

    parsed = parser.parse_args()
    if parsed.version:
        import pymule
        print(pymule.__version__)
    elif hasattr(parsed, 'func'):
        parsed.func(parsed)
    else:
        parser.error('No command specified')


if __name__ == '__main__':
    main()
