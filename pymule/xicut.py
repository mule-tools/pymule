from .loader import multiintersect, addsets, mergeset, scaleset
from scipy import optimize
import scipy.stats
import matplotlib.pyplot as plt
import matplotlib.lines
import numpy as np
from .errortools import *
from .colours import alpha_composite


def myfit(data, n):
    r"""
    performs a log-polynomial :math:`\sum_{i=0}^n a_i \log(\xi_c)^i` fit to date

    :param data:
        numpy array;
        The different :math:`\xi_c` values in the format ``np.array([[xi1, y1, e1], [xi2, y2, e2], ...])``.

    :param n:
        the degree of the polynomial

    :result:
        the coefficients and covariant matrix
    """
    xdata = np.log(data[:,0])
    ydata = data[:,1]
    edata = data[:,2]

    fitfunc = lambda p, x: sum(p[i] * x**i for i in range(len(p)))
    errfunc = lambda p, x, y, err: (y - fitfunc(p, x)) / err

    pinit = [1.0] * (n+1)
    out = optimize.leastsq(errfunc, pinit,
                           args=(xdata, ydata, edata), full_output=1)

    coeff = out[0]
    covar = out[1]

    return coeff, covar


def get_val(xs, coeff):
    r"""
    evaluates the fit obtained by :func:`~pymule.xicut.get_val`

    :param xs:
        iterable;
        values of :math:`\xi_c` to evaluate

    :param coeff:
        list;
        coefficient list ``[a_0, a_1, ..., a_n]``

    :return:
        list;
        the values of the fit at the presented values.
    """
    return [
        sum(np.log(x)**np.arange(0,len(coeff)) * coeff)
        for x in xs
    ]


def get_errorbands(x, coeff, covar, ndata, cf=0.9):
    r"""
    evaluates the errorbands of the fit obtained by :func:`~pymule.xicut.get_val`

    :param x:
        iterable;
        values of :math:`\xi_c` to evaluate

    :param coeff:
        list;
        coefficient list ``[a_0, a_1, ..., a_n]``

    :param covar:
        list;
        the covariance matrix

    :param ndata:
        int;
        the number of data points used in the fit, required for the :math:`t` value estimation

    :param cl:
        float, optional;
        the confidence level used

    :return:
        list;
        the values and errors of the fit at the presented values as ``[x, y, y-dy, y+dy]``
    """
    if type(x) == np.ndarray:
        return np.array([
            get_errorbands(i, coeff, covar, ndata, cf) for i in x
        ])
    else:
        tval = scipy.stats.t.ppf((cf+1)/2, ndata - len(coeff))
        xvec = np.log(x)**np.arange(0,len(coeff))

        delta = tval*np.sqrt(np.matmul(np.matmul(xvec, covar), xvec))
        centre = sum(xvec * coeff)

        return np.array([x, centre, centre+delta, centre-delta])


def addkeyedsets(sets):
    """
    adds list of keyed sets using :func:`~pymule.addsets`
    """
    paras = multiintersect([i.keys() for i in sets])
    return {
        para: addsets([i[para] for i in sets])
        for para in paras
    }


def xiresidue(sets, n, xlim=[-7, 0], scale=1):
    r"""
    creates a residue plot for a :math:`\xi_c` fit

    :param sets:
        dict or list;
        random-seed-merged results (usually from :func:`~pymule.sigma`) or list thereof

    :param n:
        int;
        order of the fit, 1 at NLO, 2 at NNLO

    :param xlim:
        tuple of floats, optional;
        upper and lower bounds for :math:`\log\xi_c`

    :param scale:
        float, optional;
        rescale factor for the plots

    :return:
        a figure and the fit coefficients as a matrix
    """
    if type(sets) == list:
        pset = addkeyedsets(sets)
    elif type(sets) == dict:
        pset = sets
    else:
        raise KeyError("Needs to be either list or dict")

    fig, (ax1,ax2) = plt.subplots(
        2, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [3,2]}
    )
    ax2.set_xscale('log')
    ax2.set_xlabel('$\\xi_c$')
    ax1.set_ylabel('$\\sigma^{(%d}_i$' % (n))
    ax2.set_ylabel(r'residue')
    xval = np.exp(np.linspace(xlim[0],xlim[1]))

    values = np.array([
        np.insert(scale*pset[xi]['value'], 0, xi[0])
        for xi in sorted(pset.keys())
    ])

    coeff, covar = myfit(values, n)

    band = get_errorbands(xval, coeff, covar, len(values), cf=.68)
    bandValue = get_errorbands(values[:,0], coeff, covar, len(values), cf=.68)
    dB = bandValue[:,2]-bandValue[:,3]

    ax1.plot(
        band[:,0], band[:,1],
        color='C0'
    )
    ax1.errorbar(
        values[:,0], values[:,1], abs(values[:,2]),
        linestyle='none', marker='.',
        color='C0'
    )

    ax2.axhline(0, color='black', linewidth=0.5)
    ax2.fill_between(
        band[:,0],
        band[:,2] - band[:,1],
        band[:,3] - band[:,1],
        color=alpha_composite('white', 'C0', 0.1)
    )

    ax2.errorbar(
        values[:,0],
        values[:,1] - bandValue[:,1],
        np.sqrt(values[:,2]**2 + dB**2),
        linestyle='none', marker='.',
        ecolor='C1',
        color='C0'
    )

    ax2.errorbar(
        values[:,0], values[:,1] - bandValue[:,1], abs(values[:,2]),
        linestyle='none', marker='.',
        color='C0'
    )

    ax1.legend(
        [
            matplotlib.lines.Line2D([0], [0], color='C0'),
            matplotlib.lines.Line2D(
                [0], [0], color='C0',
                marker='.', linestyle='none'
            ),
            matplotlib.lines.Line2D(
                [0], [0], color='C1',
                marker='.', linestyle='none'
            )
        ], [
            'Fit', r'$\pm1\sigma$ (stat.)', r'$\pm1\sigma$ (stat.$\oplus$fit)'
        ]
    )

    # Matplotlib's offset placing routine has a bug. This sorta fixes it
    ax2.yaxis._update_offset_text_position = lambda a,b: 0
    ax2.yaxis.offsetText.set_transform(ax2.transAxes)
    ax2.yaxis.offsetText.set_verticalalignment('top')
    ax2.yaxis.offsetText.set_position((
        0,
        0.98
    ))

    return (fig, coeff)


def mergefkswithplot(sets, scale=1., showfit=[True,True], xlim=[-7, 0]):
    r"""
    performs and FKS merge like :func:`~pymule.mergefks` but it also produces a :math:`\xi_c` independence plot

    .. note::
        In contrast :func:`~pymule.mergefks`, here phase-space partioned results need to be merged first.
        This is done by grouping those into an array first, sorted by number of particles in the final state, i.e. we start with the n-particle corrections.

    :param sets:
        list of list of  random-seed-merged results (usually from :func:`~pymule.sigma`), starting with the lowest particle number and going up

    :param scale:
        float, optional;
        rescale factor for the plot and result

    :param showfit:
        ``[bool, bool]``, optional;
        whether to show the fit lines in the overview plot (first element) and the zoomed in plot (second element)

    :param xlim:
        tuple of floats, optional;
        upper and lower bounds for :math:`\log\xi_c`

    :return:
        a figure and the FKS-merged final set containing cross sections, distributions, and run-time information.
        The chi2a return is a list of the following

         * the :math:`\chi^2` of the FKS merge
         * a list of :math:`\chi^2` from previous operations, such as random seed merging or the integration.

    :Example:
        In the partioned muon-electron scattering case

        >>> fig, res = mergefkswithplot([
        ...     [
        ...         sigma('em2emFEE'), sigma('em2emFMM'), sigma('em2emFEM')
        ...     ], [
        ...         sigma('em2emREE15'), sigma('em2emREE35'),
        ...         sigma('em2emRMM'),
        ...         sigma('em2emREM')
        ...     ]
        ... ])
    """
    n = len(sets)
    psets = [addkeyedsets(i) for i in sets]
    ans = mergeset(list(addkeyedsets(psets).values()))

    fig, (ax1,ax2) = plt.subplots(
        2, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [3,2]}
    )

    ax2.set_xscale('log')
    ax2.set_xlabel('$\\xi_c$')
    ax1.set_ylabel('$\\sigma^{(%d)}_i$' % (n-1))
    ax2.set_ylabel(
        r'$\tfrac{\sigma^{(%d)}}{\langle\sigma^{(%d)}\rangle}-1$' % (n-1, n-1)
    )
    ax2.ticklabel_format(style='sci',axis='y',scilimits=(0,0))

    xval = np.exp(np.linspace(xlim[0],xlim[1]))

    totcoeff = np.zeros(n)
    totcovar = np.zeros((n,n))
    pvalues = []
    for pset, ind in zip(psets, range(1,1+len(psets))):
        values = np.array([
            np.insert(scale*pset[xi]['value'], 0, xi[0])
            for xi in sorted(pset.keys())
        ])
        pvalues.append(values)

        coeff, covar = myfit(values, n-1)
        totcoeff += coeff
        totcovar += covar

        if showfit[0]:
            ax1.plot(
                xval, get_val(xval, coeff),
                c='C%d' % ind
            )
        ax1.errorbar(
            values[:,0], values[:,1], abs(values[:,2]),
            linestyle='none', marker='.',
            c='C%d' % ind
        )

    norm = scale*ans['value'][0]

    tot = combineNplots(addplots,pvalues)
    tot0 = [1,1/norm,1/norm] * tot - [0,1,0]

    band = get_errorbands(xval, totcoeff, totcovar, len(pvalues[0]))
    if showfit[1]:
        ax2.plot(band[:,0], band[:,1]/norm-1)
        ax2.fill_between(
            band[:,0],
            band[:,2]/norm-1, band[:,3]/norm-1,
            color=alpha_composite('white', 'C0', 0.1)
        )
    ax2.errorbar(
        tot0[:,0], tot0[:,1], abs(tot0[:,2]),
        linestyle='none', marker='.',
        c='C0'
    )

    if showfit[0]:
        ax1.plot(band[:,0], band[:,1])
    ax1.errorbar(
        tot[:,0], tot[:,1], abs(tot[:,2]),
        linestyle='none', marker='.',
        c='C0'
    )

    # Matplotlib's offset placing routine has a bug. This sorta fixes it
    ax2.yaxis._update_offset_text_position = lambda a,b: 0
    ax2.yaxis.offsetText.set_transform(ax2.transAxes)
    ax2.yaxis.offsetText.set_verticalalignment('top')
    ax2.yaxis.offsetText.set_position((
        0,
        0.98
    ))

    ax1.legend(
        ['$\\sigma^{(%d)}_{n}$' % (n-1)] +
        ['$\\sigma^{(%d)}_{n+%d}$' % (n-1, i) for i in range(1, n)] +
        ['$\\sigma^{(%d)}$' % (n-1)],
        loc='upper right'
    )

    return fig, scaleset(ans,scale)
