import argparse
import re
import sys


def create_parser(subparsers):
    parser = subparsers.add_parser(
        'ffilter',
        description='returns interfaces for functions'
    )
    parser.add_argument(
        'src', nargs='?',
        type=argparse.FileType('r'),
        default=sys.stdin
    )
    parser.add_argument(
        '--name', nargs='?',
        default=None
    )
    parser.add_argument(
        'dest', nargs='?',
        type=argparse.FileType('w'),
        default=sys.stdout
    )
    parser.add_argument(
        '--meson', action='store_true',
        default=False
    )
    parser.set_defaults(func=main)


def run(src, dest, name, fname):
    buf = src.read()
    matches = re.findall(
        'FUNCTION ([A-Z_]+[2ZXVSLW][A-Z\d_]+)(\([^\)\n]+\))\n(( *!!.*\n)+)',
        buf,
        re.I
    )
    if len(matches) > 0:
      dest.write('  !! From file %s\n' % fname)
      dest.write('\n'.join(
          '  use %s, only: %s!!%s\n%s' % (
              name, func.lower(), args, comments
          )
          for func, args, comments, _ in matches
      ))
      dest.write('\n\n')


def run_meson(build='build'):
    import subprocess
    subprocess.Popen(['ninja', '-C', build, 'mat_el_list']).communicate()
    with open(f'{build}/mat_el_list') as fp:
        groups, files = fp.read().strip().split(' | ')
        groups = groups.split(' ')
        files = files.split(' ')

    with open('src/mat_el.f95', 'w') as dest:
        dest.write("                 !!!!!!!!!!!!!!!!!!!!!!!!!\n")
        dest.write("                       MODULE MAT_EL\n")
        dest.write("                 !!!!!!!!!!!!!!!!!!!!!!!!!\n")
        dest.write("\n")
        dest.write("  use functions\n")

        for group in groups:
            dest.write(f"#ifdef HAVE_{group.upper()}\n\n")

            for f in files:
                folder, fname = f.split('/')
                if not folder == group:
                    continue
                with open(f, 'r') as src:
                    name = fname.split('.')[0]
                    run(src, dest, group, fname)

            dest.write("#endif\n")
        dest.write("                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
        dest.write("                       END MODULE MAT_EL\n")
        dest.write("                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")


def main(parsed):
    fname = parsed.src.name
    if parsed.name:
        name = parsed.name
    else:
        name = fname.split('.')[0]

    if parsed.meson:
        run_meson()
    else:
        run(parsed.src, parsed.dest, name, fname)
