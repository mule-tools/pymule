import os

try:
    ip = get_ipython()
    if ip.__class__.__name__ == 'TerminalInteractiveShell':
        ip.magic('pylab')
    elif ip.__class__.__name__ == 'ZMQInteractiveShell':
        ip.magic('pylab notebook')
except NameError:
    pass


def warn():
    import traceback
    import sys
    sys.stderr.write(
        'pymule warning: some functionality may not be available\n%s\n' % (
            traceback.format_exc()
        )
    )


__all__ = []
__version__ = '0.1.0'


if not ('PYMULE_QUICK' in os.environ and os.environ['PYMULE_QUICK'] == '1'):
    try:
        import numpy as np
        __all__ += ['np']

        from .vegas import importvegas, exportvegas
        __all__ += ['importvegas', 'exportvegas']

        from .errortools import plusnumbers, dividenumbers, timesnumbers,\
                                addplots, divideplots, scaleplot,        \
                                mergenumbers, mergeplots,                \
                                integratehistogram, mergebins,           \
                                printnumber, chisq
        __all__ += [
            'mergenumbers', 'plusnumbers', 'dividenumbers', 'timesnumbers',
            'mergeplots', 'addplots', 'divideplots', 'scaleplot',
            'integratehistogram', 'mergebins', 'printnumber',
            'chisq'
        ]

        from .loader import setup, sigma, mergefks, addsets, scaleset
        __all__ += [
            'setup', 'sigma', 'mergefks', 'addsets', 'scaleset'
        ]

        from .constants import pi, alpha, GF, conv, Mmu, Mel, Mtau
        __all__ += ['pi', 'alpha', 'GF', 'conv', 'Mmu', 'Mel', 'Mtau']

        import matplotlib.pyplot as plt
        __all__ += ['plt']

        from .xicut import mergefkswithplot, xiresidue
        __all__ += ['mergefkswithplot', 'xiresidue']

        from .plot import errorband, kplot, watermark
        __all__ += ['errorband', 'kplot', 'watermark']
        from .mule import mulify
        __all__ += ['mulify']

        from . import colours
        __all__ += ['colours']
    except Exception:
        warn()
