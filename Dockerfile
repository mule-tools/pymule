FROM yulrich/mcmule-pre:1.0.0

LABEL maintainer="yannick.ulrich@psi.ch"
LABEL version="1.0"
LABEL description="The full pymule suite"

RUN apk add --no-cache git-lfs libffi-dev openssl-dev  krb5-dev linux-headers zeromq-dev \
    py3-pygments py3-cffi py3-cryptography py3-jinja2 py3-openssl py3-pexpect \
    py3-tornado py3-pyzmq py3-defusedxml py3-entrypoints py3-attrs \
    py3-pyrsistent py3-jsonschema py3-packaging py3-webencodings py3-bleach \
    py3-prometheus-client \
    texlive texmf-dist-latexextra texlive-dvi

RUN git lfs install --skip-repo

RUN pip3 --no-cache-dir install nbconvert==5.6.1 ipython jupyter==1.0.0
RUN pip3 --no-cache-dir install git+https://gitlab.com/mule-tools/pymule.git

RUN git clone --recursive https://gitlab.com/mule-tools/user-library.git && \
    cd user-library && git config lfs.https://gitlab.com/mule-tools/user-library.git/info/lfs.locksverify true

EXPOSE 8888
WORKDIR /user-library

CMD jupyter notebook --port 8888 --no-browser --ip 0.0.0.0 --allow-root --NotebookApp.password='sha1:be71f3be1283:4ca82fe9afa35042c93372bdf7a18d68e4df5175'
